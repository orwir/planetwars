# README #

### What is this repository for? ###
It is extended sources with additional tools for testing bot for game Planet Wars http://planetwars.aichallenge.org/

**Version: 1.0**

### How do I get set up? ###

Project use ant for building and running:

**Run** - build project and run game with visual view

**Debug** - build project and run game without visual view

Also has "**examine**" folder in src where have:

**Examine.java** - run tests for bot with all bots (tools/bots) on all maps (tools/maps).