package ingvar.planetwars.tools;


import ingvar.planetwars.tools.examine.BotExamine;
import ingvar.planetwars.tools.examine.Result;
import ingvar.planetwars.tools.examine.Summary;
import ingvar.planetwars.tools.examine.TestData;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

public class Examine {

    private volatile int threadCount;
    private volatile String exec;
    private volatile boolean verbose;

    private ThreadGroup threads;
    private volatile List<TestData> data;
    private volatile List<Result> results;

    public static void main(String[] args) {
        try {
            new Examine();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Examine() throws Exception {
        threads = new ThreadGroup("examine");
        data = new LinkedList<TestData>();
        results = new LinkedList<Result>();
        loadProperties();

        for (int i = 0; i < threadCount; i++) {
            new Thread(threads, new BotExamine(this)).start();
        }
    }

    public synchronized TestData getData() {
        if (data.size() > 0) {
            return data.remove(0);
        } else {
            return null;
        }
    }

    public synchronized void addResult(Result result) {
        results.add(result);
    }

    public String getExec() {
        return exec;
    }

    public synchronized void threadExit() {
        if (threads.activeCount() > 1) {
            return;
        }
        Summary summary = new Summary(results, verbose);
        System.out.println(summary);
        try {
            FileWriter writer = new FileWriter("examine-result.txt");
            writer.write(summary.toString());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadProperties() throws Exception {
        Properties properties = new Properties();
        try {
            FileReader file = new FileReader("examine.cfg");
            properties.load(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        verbose = Boolean.valueOf(properties.getProperty("verbose", "false"));

        threadCount = Integer.parseInt(properties.getProperty("threads", "1"));
        String command = properties.getProperty("exec", "");
        if (command.isEmpty()) {
            throw new Exception("command not found in property file!");
        }
        String mybot = properties.getProperty("mybot");
        if (mybot.isEmpty()) {
            throw new Exception("mybot name not found in property file!");
        }
        exec = "cmd /c " + command.replaceAll("MYBOT", mybot);

        String[] maps = getMaps(properties.getProperty("maps", "all"), properties.getProperty("maps_dir", "./maps"));
        String[] bots = getBots(properties.getProperty("bots", "all"), properties.getProperty("bots_dir", "./bots"));
        for (String bot : bots) {
            for (String map : maps) {
                data.add(new TestData(bot, map));
            }
        }
    }

    private String[] getMaps(String names, String mapsdir) {
        if (names.equals("all")) {
            File file = new File(mapsdir);
            return file.list();
        } else {
            List<String> list = new ArrayList<String>();
            String[] m = names.split(",");
            for (String curr : m) {
                if (curr.matches("\\d+-\\d+")) {
                    String[] range = curr.split("-");
                    int start = Integer.parseInt(range[0]);
                    int end = Integer.parseInt(range[1]);
                    for (int i = start; i <= end; i++) {
                        list.add("map" + i + ".txt");
                    }
                } else if (curr.matches("\\d+")) {
                    list.add("map" + curr.trim() + ".txt");
                }
            }
            return list.toArray(new String[0]);
        }
    }

    private String[] getBots(String names, String botsdir) {
        if (names.equals("all")) {
            File file = new File(botsdir);
            return file.list();
        } else {
            return names.split(",");
        }
    }

}
