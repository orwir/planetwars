package ingvar.planetwars.tools.examine.view;

import javax.swing.*;

@SuppressWarnings("serial")
public class MainFrame extends JFrame {

    public static void main(String[] args) {
        new MainFrame().setVisible(true);
    }

    public MainFrame() {
        init();
    }

    private void init() {
        setTitle("Examine");
        setSize(640, 480);
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        add(new ExaminePanel());
    }

}
