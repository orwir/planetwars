package ingvar.planetwars.tools.examine.view;

import java.awt.*;

@SuppressWarnings("serial")
public class GBHelper extends GridBagConstraints {

    public GBHelper grid(int row, int col) {
        gridx = col;
        gridy = row;
        return this;
    }

    public GBHelper setWidth(int width) {
        gridwidth = width;
        return this;
    }

    public GBHelper setAnchor(int anchor) {
        this.anchor = anchor;
        return this;
    }

    public GBHelper setInsets(int top, int left, int bottom, int right) {
        insets = new Insets(top, left, bottom, right);
        return this;
    }

}
