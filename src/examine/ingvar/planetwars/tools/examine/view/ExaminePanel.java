package ingvar.planetwars.tools.examine.view;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class ExaminePanel extends JPanel {

    public ExaminePanel() {
        init();
    }

    private void init() {
        setLayout(new GridBagLayout());

        GBHelper helper = new GBHelper();
        helper.setAnchor(GBHelper.FIRST_LINE_START);

		/* top level: buttons panel */
        JPanel buttons = new JPanel(new FlowLayout(FlowLayout.LEFT));
        add(buttons, helper.grid(0, 0).setWidth(GBHelper.REMAINDER));

        buttons.add(new JButton("Settings"));
        buttons.add(new JLabel(">"));
        buttons.add(new JButton("My bot"));
        buttons.add(new JLabel(">"));
        buttons.add(new JButton("Enemies"));
        buttons.add(new JLabel(">"));
        buttons.add(new JButton("Maps"));
        buttons.add(new JLabel(">"));
        buttons.add(new JButton("Run"));
		
		/* middle level: summary/filtering */
        JPanel summary = new JPanel(new GridBagLayout());
        add(summary, helper.grid(1, 0).setWidth(GBHelper.RELATIVE).setInsets(5, 5, 0, 0));
        summary.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Summary"));
        summary.setPreferredSize(new Dimension(150, 120));

        helper.weighty = 1;
        helper.weightx = 1;

        helper.setWidth(GBHelper.RELATIVE).setAnchor(GBHelper.FIRST_LINE_START).setInsets(0, 5, 5, 0);
        summary.add(new JLabel("Battles:"), helper.grid(0, 0));
        summary.add(new JLabel("Wins:"), helper.grid(1, 0));
        summary.add(new JLabel("Defeats:"), helper.grid(2, 0));
        summary.add(new JLabel("Timeouts:"), helper.grid(3, 0));
        summary.add(new JLabel("100"), helper.grid(0, 1));
        summary.add(new JLabel("90 (90%)"), helper.grid(1, 1));
        summary.add(new JLabel("7 (7%)"), helper.grid(2, 1));
        summary.add(new JLabel("3 (3%)"), helper.grid(3, 1));

        JPanel filters = new JPanel(new GridBagLayout());
        add(filters, helper.grid(1, 1).setWidth(GBHelper.RELATIVE).setInsets(5, 0, 0, 0));
        filters.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Filters"));
        filters.setPreferredSize(new Dimension(150, 120));

        helper.setWidth(GBHelper.RELATIVE).setAnchor(GBHelper.FIRST_LINE_START).setInsets(0, 5, 5, 0);
        filters.add(new JCheckBox("Wins"), helper.grid(0, 0));
        filters.add(new JCheckBox("Defeats"), helper.grid(1, 0));
        filters.add(new JCheckBox("Timeouts"), helper.grid(2, 0));
        filters.add(new JLabel(" "), helper.grid(3, 0));
		
		/*bottom level: results table*/
    }

}
