package ingvar.planetwars.tools.examine;

import ingvar.planetwars.tools.Examine;

import java.io.BufferedInputStream;
import java.io.IOException;

public class BotExamine implements Runnable {

    private final Examine examine;

    public BotExamine(Examine examine) {
        this.examine = examine;
    }

    @Override
    public void run() {
        TestData data;
        while ((data = examine.getData()) != null) {
            try {
                String exec = examine.getExec().replaceAll("MAP", data.map).replaceAll("ENEMY", data.bot);
                Process proc = Runtime.getRuntime().exec(exec);

                BufferedInputStream in = new BufferedInputStream(proc.getErrorStream());
                StringBuilder output = new StringBuilder();
                int c;
                while ((c = in.read()) != 33) {
                    output.append((char) c);
                }
                output.append("!");
                Result result;
                try {
                    result = Result.getResult(data, output.toString());
                } catch (Exception e) {
                    result = new Result(data.map, data.bot, 0, false, true,
                            String.format("msg: [%s]; out: [%s]", e.getMessage(),
                                    output.toString().replaceAll(System.getProperty("line.separator"), " ")));
                }
                examine.addResult(result);
                in.close();
                proc.destroy();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        examine.threadExit();
    }

}
