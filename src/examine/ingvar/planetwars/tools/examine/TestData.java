package ingvar.planetwars.tools.examine;

public class TestData {

    public final String bot;
    public final String map;

    public TestData(String bot, String map) {
        this.bot = bot;
        this.map = map;
    }

}
