package ingvar.planetwars.tools.examine;

import java.util.List;

public class Summary {

    private List<Result> results;
    private float battles;
    private int wins;
    private int losses;
    private int timeouts;
    private int errors;
    private boolean verbose;

    public Summary(List<Result> results, boolean verbose) {
        this.results = results;
        this.verbose = verbose;
        battles = results.size();
        wins = 0;
        losses = 0;
        timeouts = 0;
        errors = 0;
        for (Result r : results) {
            if(!r.error().isEmpty()) {
                errors++;
            } else if (r.win()) {
                wins++;
            } else if(r.timeout()) {
                timeouts++;
            } else {
                losses++;
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Battles: ").append(String.format("%.0f", battles)).append("\n");
        sb.append("Wins: ").append(wins).append(" (").append(percent(wins / battles * 100)).append("%)").append("\n");
        sb.append("Losses: ").append(losses).append(" (").append(percent(losses / battles * 100)).append("%)").append("\n");
        sb.append("Timeouts: ").append(timeouts).append(" (").append(percent(timeouts / battles * 100)).append("%)").append("\n");
        sb.append("Errors: ").append(errors).append(" (").append(percent(errors / battles * 100)).append("%)").append("\n");

        if(verbose) {
            sb.append("\n");
            sb.append("Lost matches:\n");
            for(Result r : results) {
                if(r.error().isEmpty() && !r.win() && !r.timeout()) {
                    sb.append("Enemy: [").append(r.bot()).append("]; Map: [").append(r.map());
                    sb.append("]; Turn: [").append(r.turns()).append("]\n");
                }
            }

            sb.append("\nTimeout matches:\n");
            for(Result r : results) {
                if(r.error().isEmpty() && r.timeout()) {
                    sb.append("Enemy: [").append(r.bot()).append("]; Map: [").append(r.map());
                    sb.append("]; Turn: [").append(r.turns()).append("]\n");
                }
            }

            sb.append("\nError matches:\n");
            for(Result r : results) {
                if(!r.error().isEmpty()) {
                    sb.append("Enemy: [").append(r.bot()).append("]; Map: [").append(r.map());
                    sb.append("]; Turn: [").append(r.turns()).append("]; Error: ").append(r.error()).append("\n");
                }
            }
        }

        return sb.toString();
    }

    private String percent(float percent) {
        return String.format("%.0f", percent);
    }

}
