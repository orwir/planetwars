package ingvar.planetwars.tools.examine;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Result {

    private static final Pattern SUCCESS = Pattern.compile("(Turn \\d+\\s)+\\s(Draw!|Player \\d+ Wins!)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    private static final Pattern TIMEOUT = Pattern.compile("WARNING: player \\d+ timed out.", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    private static final Pattern LAST_TURN = Pattern.compile("(?<=Turn\\s)\\d+(?=\\s+(Player|Draw))", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
    private static final Pattern WINNER = Pattern.compile("(?<=Player )\\d+(?= Wins!)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);

    private final String error;
    private final String map;
    private final String bot;
    private final int turns;
    private final boolean win;
    private final boolean timeout;

    public static Result getResult(TestData data, String output) {
        if (!SUCCESS.matcher(output).find()) {
            return new Result(data.map, data.bot, 0, false, false, output);
        }

        boolean timeout = TIMEOUT.matcher(output).find();

        Matcher mLastTurn = LAST_TURN.matcher(output);
        int turns = -1;
        if(mLastTurn.find()) {
            turns = Integer.parseInt(mLastTurn.group());
        }

        Matcher mWinner = WINNER.matcher(output);
        boolean win = false;
        if(mWinner.find()) {
            win = Integer.parseInt(mWinner.group()) == 1;
        }

        return new Result(data.map, data.bot, turns, win, timeout, "");
    }

    public Result(String map, String bot, int turns, boolean win, boolean timeout, String error) {
        this.map = map;
        this.bot = bot;
        this.turns = turns;
        this.win = win;
        this.timeout = timeout;
        this.error = error;
    }

    public String map() {
        return map;
    }

    public String bot() {
        return bot;
    }

    public int turns() {
        return turns;
    }

    public boolean win() {
        return win;
    }

    public boolean timeout() {
        return timeout;
    }

    public String error() {
        return error;
    }

}
