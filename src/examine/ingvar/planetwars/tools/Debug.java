package ingvar.planetwars.tools;

import planetwars.MyBot;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Debug {

    private static final String regex = "(?<=engine > player1: )[^go][\\w\\s.]+(?=go)";

    public static void main(String[] args) {
        Pattern pattern = Pattern.compile(regex);
        try {
            Matcher matcher = pattern.matcher(parseFile());
            while (matcher.find()) {
                String s = matcher.group();
                MyBot.main(new String[]{s});
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String parseFile() throws IOException {
        StringBuilder sb = new StringBuilder();
        FileInputStream file = new FileInputStream("log.txt");
        while (file.available() > 0) {
            sb.append((char) file.read());
        }
        return sb.toString();
    }

}
